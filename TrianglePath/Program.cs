﻿using System;
using System.Collections.Generic;
using System.IO;

namespace TrianglePath
{
    class Program
    {
        static int GetMaxPath(string filename)
        {
            try
            {
                List<List<int>> data = new List<List<int>>();

                //Запись данных из файла filename в матрицу data
                using (var sr = new StreamReader(filename))
                {
                    string[] row;
                    while ((row = sr.ReadLine()?.Split()) != null)
                    {
                        List<int> added = new List<int>();
                        foreach (var num in row)
                        {
                            added.Add(Convert.ToInt32(num));
                        }
                        data.Add(added);
                    }
                }

                //C (n-1)-й по 1-ю строку меняем значения ячеек на максимальную сумму пути от этой ячейки до конца треугольника
                for (int i = data.Count - 2; i >= 0; i--)
                {
                    data[i][0] = Math.Max(data[i + 1][0], data[i + 1][1]);
                    for (int j = 1; j < data[i].Count; j++)
                    {
                        data[i][j] += Math.Max(Math.Max(data[i + 1][j - 1], data[i + 1][j]), data[i + 1][j + 1]);
                    }
                }

                //В итоге в 1-й ячейке будет записана сумма пути от начала до конца треугольника
                return data[0][0];
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return -1;
            }
        }

        static void Main(string[] args)
        {
            Console.WriteLine("First triangle sum: " + GetMaxPath(@"..\..\first_example.txt").ToString());
            Console.WriteLine("Second triangle sum: " + GetMaxPath(@"..\..\second_example.txt").ToString());
            Console.ReadKey();
        }
    }
}
