﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CircularPrime
{
    class Program
    {
        static SortedSet<int> primes = new SortedSet<int>();

        //поиск простых чисел от 3 до limit с помощью решета Эратосфена, 
        //изначально учитываются только нечетные числа
        static void SetPrimes(int limit)
        {
            int sieveLength = (limit - 1) / 2;
            int sqrtLimit = ((int)Math.Sqrt(limit) - 1) / 2;

            //i-е значение в primeBits показывает, является ли простым число 2 * i + 1
            BitArray primeBits = new BitArray(sieveLength + 1, true);
            for (int i = 1; i <= sqrtLimit; i++)
            {
                if (primeBits.Get(i))
                {
                    for (int j = i * 2 * (i + 1); j <= sieveLength; j += 2 * i + 1)
                    {
                        primeBits.Set(j, false);
                    }
                }
            }

            for (int i = 1; i <= sieveLength; i++)
            {
                if (primeBits.Get(i))
                {
                    primes.Add(2 * i + 1);
                }
            }
        }

        static int CheckCircularPrime(int prime)
        {
            int exponent = 0;
            int tempPrime = prime;
            int d;

            //отбрасываются числа, которые содержат четные цифры или 5, 
            //т.к. число, заканчивающееся на них, не может быть простым;
            while (tempPrime > 0)
            {
                d = tempPrime % 10;
                if (d % 2 == 0 || d == 5)
                {
                    primes.Remove(prime);
                    return 0;
                }
                tempPrime /= 10;
                exponent++;
            }

            //Сдвиг числа и проверка, являются ли полученные числа простыми
            int swap = (int)Math.Pow(10, exponent - 1);
            tempPrime = prime;
            List<int> foundCircularPrimes = new List<int>();

            for (int i = 0; i < exponent; i++)
            {
                if (primes.Contains(tempPrime))
                {
                    foundCircularPrimes.Add(tempPrime);
                    primes.Remove(tempPrime);
                }
                else if (!foundCircularPrimes.Contains(tempPrime))
                {
                    return 0;
                }

                d = tempPrime % 10;
                tempPrime = d * swap + tempPrime / 10;
            }

            return foundCircularPrimes.Count;
        }

        static void Main(string[] args)
        {
            SetPrimes(1000000);

            //учитываем отсутствующие в primes 2 (т.к. поиск простых чисел шел с 3) и 5
            //в функции CheckCircularPrime отбрасываются числа, которые содержат цифры 2 или 5, 
            //т.к. число, заканчивающееся на них, не может быть простым;
            //исключение - заранее учтенные в сумме числа 5 и 2
            int sum = 2;

            while (primes.Count > 0)
            {
                sum += CheckCircularPrime(primes.Min);
            }

            Console.WriteLine("Circular primes below 1,000,000: " + sum.ToString());
            Console.ReadKey();
        }
    }
}
